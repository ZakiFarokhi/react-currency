import React from 'react'
import exchange from './exchange.png'
class Currency extends React.Component{
    constructor(props){
        super(props)
        //this.currencies = []
        this.cached = {}
        this.state = {
            base: "USD",
            target: "EUR",
            value: 0,
            converted: 0,
            currencies: []
        }
    }
    render(){
        return(
        <div>
            <div className="exchange"> 
            <div style={{marginTop:"20px"}}>
                <input style={{width:"150px", height:"30px"}} type="number" value={this.state.value} onChange={this.changeInputOne} />   
                <select style={{width:"90px", height:"40px"}} onChange={this.makeSelection} name="base" value={this.state.base}>
                {this.state.currencies.map(currency => <option key={currency} value={currency}>{currency}</option>)}    
                </select>
                <img onClick={this.tukar} src={exchange} alt="exchange-icon" style={{width: "30px", height: "30px"}}></img>
                <select style={{width:"90px", height:"40px"}} onChange={this.makeSelection} name="target" value={this.state.target}>
                {this.state.currencies.map(currency => <option key={currency} value={currency}>{currency}</option>)}    
                </select>
            </div>   
            <div>
                <h4>{this.state.value}  {this.state.base} = </h4>
                <h3 type="number" disabled={true}>{this.state.converted === null? "Calculating..." : this.state.converted}  {this.state.target}</h3>
            </div>  
            </div> 
        </div>
        )
    }
    tukar = () => {
        this.setState({
            base : this.state.target,
            target : this.state.base
        },this.recalculate )
    }      
    makeSelection = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        }, this.recalculate )
    }
    changeInputOne = (event) => {
        this.setState({
            value: event.target.value,
        }, this.recalculate )
    }
    recalculate = () => {
        const newValue = parseFloat(this.state.value);
        if (isNaN(newValue)){
            return;
        }
        if(this.cached[this.state.base] !== undefined && Date.now() - this.cached[this.state.base].timestamp < (1000 * 60)){
            this.setState({
                converted: this.cached[this.state.base].rates[this.state.target] * newValue
            })
            return;
        }

        fetch(`https://api.exchangeratesapi.io/latest?base=${this.state.base}`)
        .then(response => response.json())
        .then(data => {


            this.cached[this.state.base] = {
                rates: data.rates,
                timestamp: Date.now()
            }

            this.setState ({
                converted: data.rates[this.state.target] * newValue
            })
        })
    }
    componentDidMount = () =>{
        fetch(`https://api.exchangeratesapi.io/latest?base=${this.state.base}`)
        .then(response => response.json())
        .then(data => {
            console.log(data)
            this.setState ({
                currencies: [...Object.keys(data.rates), "EUR"]
            })
                //this.currencies = [...Object.keys(data.rates), "EUR"]
            
           
        })
        
    }

}
export default Currency;