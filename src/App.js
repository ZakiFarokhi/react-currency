import React from 'react';
import logoc from './currency-logo.png'
import './App.css';
import Currency from './Currency'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logoc} className="App-logo" alt="logo" />
        <Currency></Currency>
      </header>
    </div>
  );
}

export default App;
